package com.serge_app.bethebanker;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;

public class ScrollListActivity extends AppCompatActivity {

    String image;

/*rivate String ChooseBoy(View view)
{


    return image;
}

    private String ChooseGirl(View view)
    {
        String image = String.valueOf(R.drawable.woman_trader);

        SharedPreferences.Editor editor =getSharedPreferences("Image_chosen", MODE_PRIVATE).edit();

        editor.putString("Image_chosen" , image);

        return image;
    }*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

       /* ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);*/

        MenuInflater menuInflater = new MenuInflater(this);

        menuInflater.inflate(R.menu.bethebankermenu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        if(item.getItemId()==R.id.GraphPage)
        {

            Intent graphintent = new Intent(this,GraphActivity.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Disclaimer)
        {
            Intent graphintent = new Intent(this,PolitiqueConfidentalite.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.MainPage)
        {
            Intent scrollList = new Intent(this,ActivityChoice.class);
            startActivity(scrollList);


        }

        if(item.getItemId() == R.id.Logout) {

            ParseUser.logOut();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.Destroy) {

            Intent intent = new Intent(getApplicationContext(),DestroyUser.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.RecapReceive) {

            Intent intent = new Intent(getApplicationContext(),RecapMenu.class);

            startActivity(intent);

        }



        return super.onOptionsItemSelected(item);
    }

    public void comp_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ScrollListActivity.class);
        startActivity(graphintent);

    }

    public void graph_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), GraphActivity.class);
        startActivity(graphintent);

    }

    public void settings_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), UpdateMail.class);
        startActivity(graphintent);

    }

    public void mail_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), RecapMenu.class);
        startActivity(graphintent);

    }

    public void backarrow(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ActivityChoice.class);
        startActivity(graphintent);

    }
    Integer maxscore;

    Integer c = 0;

    String theirimage;

ArrayList<Person> PersonList = new ArrayList<Person>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_list);


        Log.i("IMAGE WOMAN TRADER", String.valueOf(R.drawable.woman_trader));





        if (ParseUser.getCurrentUser().getString("Image_chosen") == null) {

            final AlertDialog builder = new AlertDialog.Builder(this).create();

            LayoutInflater dialog_choose_inflater = (LayoutInflater) ScrollListActivity.this.getSystemService(ScrollListActivity.this.LAYOUT_INFLATER_SERVICE);

            final View choose_view = dialog_choose_inflater.inflate(R.layout.dialog_choose_image, null);

            builder.setView(choose_view);

            builder.setTitle(" Choisissez votre personnage");

            builder.show();

            ImageView imgButton = (ImageView) choose_view.findViewById(R.id.imageButtonMan);
            imgButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    image = String.valueOf(R.drawable.trader_male_same);

                    ParseUser.getCurrentUser().put("Image_chosen", image);

                    ParseUser.getCurrentUser().saveInBackground();


                    builder.dismiss();

                    Log.i("IMAGE TRADER MALE ", image);


                    final ListView listView = findViewById(R.id.ScrollListView);

                    ViewGroup headerView = (ViewGroup) getLayoutInflater().inflate(R.layout.header_layout, listView, false);
                    // Add header view to the ListView
                    listView.addHeaderView(headerView);

                    ParseQuery<ParseUser> query = ParseUser.getQuery();

                    query.orderByDescending("Score");


                    query.findInBackground(new FindCallback<ParseUser>() {
                        @Override
                        public void done(List<ParseUser> objects, ParseException e) {
                            c = 0;
                            for (ParseUser ob : objects) {
                                c = c + 1;

                                maxscore = 0;
                                Log.i("Person", ob.getUsername());


                                if ((ob.getString("Image_chosen") == null)) {
                                    Log.i("flag", "null img");

                                    theirimage = String.valueOf(R.drawable.voliens_logo);

                                } else {

                                    if (ob.getString("Image_chosen").isEmpty() == true) {

                                        Log.i("flag", "empty img");

                                        theirimage = String.valueOf(R.drawable.voliens_logo);
                                    } else {

                                        theirimage = ob.getString("Image_chosen");

                                        Log.i("flag image", theirimage);
                                    }
                                }

                                if (ob.getUsername().equals(ParseUser.getCurrentUser().getUsername()) ){

                                    theirimage = image;
                                }


                                List<Integer> scores = ob.getList("Score");

                                if (scores != null) {
                                    if ((scores.contains(null) == false) || (scores.contains("") == false)) {

                                        Log.i("sores", String.valueOf(scores.contains(null)));

                                        if (scores.size() > 0) {
                                            try {
                                                maxscore = Collections.max(scores);
                                            } catch (Exception ClassCastException) {

                                                maxscore = 0;
                                            }

                                        }

                                    }
                                }


                                Log.i("theirimage", theirimage);

                                PersonList.add(new Person(ob.getUsername(), maxscore, "drawable://" + theirimage, c));


                            }

                            PersonListAdapter adapter = new PersonListAdapter(getApplicationContext(), R.layout.scroll_list_view, PersonList);
                            // ListView mListView = (ListView) findViewById(R.id.ScrollListView);

                            //ArrayAdapter ar = new ArrayAdapter(getApplicationContext(),R.layout);

                            Log.i("adapter", adapter.toString());

                            listView.setAdapter(adapter);


                        }

                        //PersonListAdapter adapter = new PersonListAdapter(getApplicationContext(),R.layout.scroll_list_view,PersonList);


                    });




                    PersonListAdapter adapter = new PersonListAdapter(getApplicationContext(), R.layout.scroll_list_view, PersonList);
                    // ListView mListView = (ListView) findViewById(R.id.ScrollListView);

                    //ArrayAdapter ar = new ArrayAdapter(getApplicationContext(),R.layout);



                    listView.setAdapter(adapter);
                }

            });

            ImageView imgButton2 = (ImageView) choose_view.findViewById(R.id.imageButtonwoman);
            imgButton2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    image = String.valueOf(R.drawable.woman_trader);

                    ParseUser.getCurrentUser().put("Image_chosen", image);

                    ParseUser.getCurrentUser().saveInBackground();


                    builder.dismiss();


                    final ListView listView = findViewById(R.id.ScrollListView);

                    ViewGroup headerView = (ViewGroup) getLayoutInflater().inflate(R.layout.header_layout, listView, false);
                    // Add header view to the ListView
                    listView.addHeaderView(headerView);

                    ParseQuery<ParseUser> query = ParseUser.getQuery();

                    query.orderByDescending("Score");


                    query.findInBackground(new FindCallback<ParseUser>() {
                        @Override
                        public void done(List<ParseUser> objects, ParseException e) {
                            c = 0;
                            for (ParseUser ob : objects) {
                                c = c + 1;

                                maxscore = 0;
                                Log.i("Person", ob.getUsername());


                                if ((ob.getString("Image_chosen") == null)) {
                                    Log.i("flag3", "null img");

                                    theirimage = String.valueOf(R.drawable.voliens_logo);

                                } else {

                                    if (ob.getString("Image_chosen").isEmpty() == true) {

                                        Log.i("flag2", "empty img");

                                        theirimage = String.valueOf(R.drawable.voliens_logo);
                                    } else {

                                        theirimage = ob.getString("Image_chosen");

                                        Log.i("flag image2", theirimage);
                                    }
                                }

                                if (ob.getUsername().equals(ParseUser.getCurrentUser().getUsername()) ){

                                    theirimage = image;
                                }

                                List<Integer> scores = ob.getList("Score");

                                if (scores != null) {
                                    if ((scores.contains(null) == false) || (scores.contains("") == false)) {

                                        Log.i("sores", String.valueOf(scores.contains(null)));

                                        if (scores.size() > 0) {
                                            try {
                                                maxscore = Collections.max(scores);
                                            } catch (Exception ClassCastException) {

                                                maxscore = 0;
                                            }

                                        }

                                    }
                                }




                                PersonList.add(new Person(ob.getUsername(), maxscore, "drawable://" + theirimage, c));



                            }

                            PersonListAdapter adapter = new PersonListAdapter(getApplicationContext(), R.layout.scroll_list_view, PersonList);
                            // ListView mListView = (ListView) findViewById(R.id.ScrollListView);

                            //ArrayAdapter ar = new ArrayAdapter(getApplicationContext(),R.layout);

                            Log.i("adapter", adapter.toString());

                            listView.setAdapter(adapter);


                        }

                        //PersonListAdapter adapter = new PersonListAdapter(getApplicationContext(),R.layout.scroll_list_view,PersonList);


                    });




                    PersonListAdapter adapter = new PersonListAdapter(getApplicationContext(), R.layout.scroll_list_view, PersonList);
                    // ListView mListView = (ListView) findViewById(R.id.ScrollListView);

                    //ArrayAdapter ar = new ArrayAdapter(getApplicationContext(),R.layout);


                    listView.setAdapter(adapter);

                }
            });

        } else {


            image = ParseUser.getCurrentUser().getString("Image_chosen");

            Log.i("IMAGEsharde ", image);



            final ListView listView = findViewById(R.id.ScrollListView);

            ViewGroup headerView = (ViewGroup) getLayoutInflater().inflate(R.layout.header_layout, listView, false);
            // Add header view to the ListView
            listView.addHeaderView(headerView);

            ParseQuery<ParseUser> query = ParseUser.getQuery();

            query.orderByDescending("Score");


            query.findInBackground(new FindCallback<ParseUser>() {
                @Override
                public void done(List<ParseUser> objects, ParseException e) {
                    c = 0;
                    for (ParseUser ob : objects) {
                        c = c + 1;

                        maxscore = 0;
                        Log.i("Person", ob.getUsername());


                        if ((ob.getString("Image_chosen") == null)) {
                            Log.i("flag", "null img");

                            theirimage = String.valueOf(R.drawable.parrain_icon);

                        } else {

                            if (ob.getString("Image_chosen").isEmpty() == true) {

                                Log.i("flag", "empty img");

                                theirimage = String.valueOf(R.drawable.parrain_icon);
                            } else {

                                theirimage = ob.getString("Image_chosen");

                                Log.i("flag image", theirimage);
                            }
                        }



                        List<Integer> scores = ob.getList("Score");

                        if (scores != null) {
                            if ((scores.contains(null) == false) || (scores.contains("") == false)) {

                                Log.i("sores", String.valueOf(scores.contains(null)));

                                if (scores.size() > 0) {
                                    try {
                                        maxscore = Collections.max(scores);
                                    } catch (Exception ClassCastException) {

                                        maxscore = 0;
                                    }

                                }

                            }
                        }



                        PersonList.add(new Person(ob.getUsername(), maxscore, "drawable://" + theirimage, c));
                        Log.i("PersonList1", PersonList.toString());


                    }

                    PersonListAdapter adapter = new PersonListAdapter(getApplicationContext(), R.layout.scroll_list_view, PersonList);
                    // ListView mListView = (ListView) findViewById(R.id.ScrollListView);

                    //ArrayAdapter ar = new ArrayAdapter(getApplicationContext(),R.layout);


                    listView.setAdapter(adapter);


                }

                //PersonListAdapter adapter = new PersonListAdapter(getApplicationContext(),R.layout.scroll_list_view,PersonList);


            });



            PersonListAdapter adapter = new PersonListAdapter(getApplicationContext(), R.layout.scroll_list_view, PersonList);
            // ListView mListView = (ListView) findViewById(R.id.ScrollListView);

            //ArrayAdapter ar = new ArrayAdapter(getApplicationContext(),R.layout);



            listView.setAdapter(adapter);
        }





        }



}
