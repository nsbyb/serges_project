package com.serge_app.bethebanker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.ParseACL;
import com.parse.ParseAnalytics;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.HashMap;

//import android.support.v7.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {



    public void LoginMethod (View view)
    {


        EditText edittext = (EditText) findViewById(R.id.PasswordId);
        final String password = edittext.getText().toString();
        //Log.i("password",password);

        EditText UsernameEditText = (EditText) findViewById(R.id.Userid);
        final String Username = UsernameEditText.getText().toString().replace(" ","");


        //Log.i("current User:",ParseUser.getCurrentUser().getUsername().toString());

       //ParseObject banker = new ParseObject("Banker");










       // ParseUser new_user = new ParseUser();
        ParseUser.logInInBackground(Username, password, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {

                if(e == null)
                {
                    //Toast.makeText(MainActivity.this, "Successfully Logged in",Toast.LENGTH_LONG);

                    HashMap<String,ParseUser> request = new HashMap<String,ParseUser>();
                    //request.put("user",ParseUser.getCurrentUser());




                    redirectifLogin();
                }
                else
                {
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                    // Toast.makeText(MainActivity.this, "Mot de passe ou nom d'utilisateur non reconnu , inscrivez vous s'il vous plait", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }


    public void ForgottenPassword(View view) {

    /*    EditText nametext = findViewById(R.id.Userid);
        String user = nametext.getText().toString();
        Log.i("Stringuser",user);

        if (user != null) {

            ParseQuery<ParseUser> query = ParseUser.getQuery();
            query.whereEqualTo("username", user);


            query.findInBackground( new FindCallback<ParseUser>() {
                @Override
                public void done(List<ParseUser> listParse, ParseException e) {

                    ParseUser parseObject = listParse.get(0);

                    Log.i("Username",parseObject.getUsername().toString());

                    String MailString = parseObject.getEmail();



                    if (MailString != null) {
                        ParseUser.requestPasswordResetInBackground(MailString,
                                new RequestPasswordResetCallback() {
                                    public void done(ParseException e) {
                                        if (e == null) {
                                            Toast.makeText(getApplicationContext(), "Changement de mot de passe envoyé", Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(getApplicationContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();

                                        }
                                    }
                                });
                    } else {

                        Toast.makeText(getApplicationContext(), "Votre e mail n'a pas été renseigné", Toast.LENGTH_LONG).show();

                    }

                }
            });

        }
        else{

            Toast.makeText(getApplicationContext(), "Entrez votre nom d'utilisateur puis recommencez",Toast.LENGTH_LONG).show();;
        }*/
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("UserData", MODE_PRIVATE);
        String pwd = prefs.getString("Password","");

     /*   if(pwd !=null)
        {

            Log.i("PWD NOT NULL","");
        }
        if(pwd ==null)
        {

            Log.i("PWD IS NULL","");
        }
*/


        Toast.makeText(getApplicationContext(),"Connectez vous puis changez de mot de passe ,dernier onglet",Toast.LENGTH_LONG).show();;
        EditText edittext = (EditText) findViewById(R.id.PasswordId);
       edittext.setText(pwd);




    }








    public void SignUpMethod(View view) {

        EditText edittext = (EditText) findViewById(R.id.PasswordId);
        final String password = edittext.getText().toString();



        EditText UsernameEditText = (EditText) findViewById(R.id.Userid);
        final String Username = UsernameEditText.getText().toString();


        if ( (password.equals("" ) )| (Username.equals(""))) {

            Intent intent = new Intent(getApplicationContext(),Inscription.class);

            startActivity(intent);



        }

        if ( (!password.equals("")) & (!Username.equals("")))

        {

        final ParseUser user_signup = new ParseUser();
        user_signup.setPassword(password);
        user_signup.setUsername(Username);


            HashMap<String,String> params = new HashMap<String,String>();
            params.put("username",Username);
            //request.put("user",ParseUser.getCurrentUser());

            ParseCloud.callFunctionInBackground("CreateUser",params,  new FunctionCallback<String>() {
                public void done(String str, ParseException e) {
                    if (e == null) {

                        Log.i("user","Usercreated");
                    }
                }

            });

        user_signup.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {


                if (e == null) {


                    SharedPreferences prefs = getSharedPreferences("UserData", MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    if(! prefs.contains("Password")) {
                        editor.putString("Password", password);
                        editor.commit();
                    }
                    if(! prefs.contains("username")) {
                        editor.putString("username", user_signup.getUsername().toString());

                        //editor.putString("password", password);
                        editor.commit();

                        //ParseUser.getCurrentUser().put()
                        Toast.makeText(MainActivity.this, "Inscription effectuée avec succès", Toast.LENGTH_LONG).show();

                        ParseACL postACL = new ParseACL(ParseUser.getCurrentUser());
                        postACL.setPublicReadAccess(true);
                        postACL.setPublicWriteAccess(false);
                        ParseUser.getCurrentUser().setACL(postACL);
                        ParseUser.getCurrentUser().saveInBackground();

                        redirectifSignedUp();
                    }
                   if(prefs.contains("username")){
                        // A enlever ligne ci dessous :
                        //redirectifSignedUp();

                        Toast.makeText(getApplicationContext() , "Vous ne pouvez pas avoir plus de deux comptes",Toast.LENGTH_LONG).show();

                       user_signup.logOut();
                    }

                } else {

                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        });

    }


    }

    public void redirectifLogin() {

        if (ParseUser.getCurrentUser() != null) {


            Intent intent = new Intent(getApplicationContext(),ActivityChoice.class);
            //intent.putExtra("flag",0);
            startActivity(intent);

        }
    }

    public void redirectifSignedUp() {

        if (ParseUser.getCurrentUser() != null) {
            Intent intent = new Intent(getApplicationContext(),Beginner_Activity.class);

            //intent.putExtra("flag",1);

            startActivity(intent);

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText edit_user = (EditText) findViewById(R.id.Userid);

        SharedPreferences prefs = getApplicationContext().getSharedPreferences("UserData", MODE_PRIVATE);
        String username = prefs.getString("username","");

        edit_user.setText(username);



        ParseAnalytics.trackAppOpenedInBackground(getIntent());





/*
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
    }
}
