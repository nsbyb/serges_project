package com.serge_app.bethebanker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.parse.ParseUser;

public class PolitiqueConfidentalite extends Activity {


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

       /* ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);*/

        MenuInflater menuInflater = new MenuInflater(this);

        menuInflater.inflate(R.menu.bethebankermenu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        if(item.getItemId()==R.id.MainPage)
        {

            Intent graphintent = new Intent(this,ActivityChoice.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Disclaimer)
        {
            Intent graphintent = new Intent(this,PolitiqueConfidentalite.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Classement)
        {
            Intent scrollList = new Intent(this,ScrollListActivity.class);
            startActivity(scrollList);


        }

        if(item.getItemId() == R.id.Logout) {

            ParseUser.logOut();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.Destroy) {

            Intent intent = new Intent(getApplicationContext(),DestroyUser.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.RecapReceive) {

            Intent intent = new Intent(getApplicationContext(),RecapMenu.class);

            startActivity(intent);

        }

  /*      if(item.getItemId() == R.id.UploadMail) {

            Intent intent = new Intent(getApplicationContext(),UpdateMail.class);

            startActivity(intent);

        }*/


        return super.onOptionsItemSelected(item);
    }

    public void BacktoMainPage(View view)
    {
        Intent graphintent = new Intent(this,ActivityChoice.class);
        startActivity(graphintent);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_politique_confidentalite);

        TextView tv = findViewById(R.id.BeginnerText);

        String str =
                "<font color=#1F2359><u><b>Politique d'Utilisation des données et Règlement:</b></u></font>" +
                        "<br>"+
                        "<br><b>I-Informations relatives au compte :</b></br>" +
                        "<small>"+
                        "                <br><li>VosLiens associe toutes vos informations à votre Compte et votre nom d’utilisateur.</br></li>" +
                        "                 <li>Cela concerne les informations reçues de tous les appareils mobiles exécutant l’Application, sur lesquels vous vous êtes connecté à l’aide de votre nom d’utilisateur et votre mot de passe.</br></li>" +
                        "                 <li>Les informations visibles à tout moment par les autres utilisateurs de l'application (hors réception des données de parrainage) sont: votre score et votre nom d'utilisateur.</li>" +
                        "                <li>Les informations que vous souhaitez diffuser au moment de l'envoi de vos liens de parrainage résultent de votre entière responsabilité.</li>" +
                        "                 <li>Vous êtes entièrement responsable de l’utilisation ou de l’usage abusif de votre Compte et des détails personnels résultant de la divulgation de vos informations de connexion à autrui.</li> "+
                        "                <li>Vous devez préserver la confidentialité absolue de vos informations de connexion et vous abstenir de les divulguer à d’autres personnes.</li>" +
                        "                <li>Veillez à changer votre mot de passe régulièrement.</li> "+"</small>"+

                        "    <big> <u> <br><b> II-Informations relative à la collecte des données:</b></br></u></big>"+
                        "                <br><li> VosLiens collecte les données que vous choisissez de partager avec les autres utilisateurs (notamment votre nom , prénom ,l'entité de parraiange, code de parrainage etc..." +
                        "                ,en fonction des informations demandées par l'entité responsable du parrainage, comme l'adresse e mail pour certaines.)" +
                        "               <br><li> De plus , sont stockées les dates d'emission et de réception de vos demandes de parrainage, votre nom d'utilisateur et mot de passe." +
                        "                <br><li>Les autres données collectées par VosLiens telles que les dates d'émission et de réception des envois de parrainage servent au bon" +
                        "                fonctionnement de l'application (calcul du score par exemple) et ne sont pas utilisées à des fins commerciales." +
                        "                <br><li>Les données sont stockées sur server Amazon EC2 , Ohio , USA (pas d'équivalent GDPR)." +
                        "                <br><li>Vos informations sont conservées jusqu'à votre demande de suppression de compte(voir chapitre VIII)" +
                        ""+ "<br>"+
                        "               <big><u><br><b> III-Métadonnées </b></u></big>" +

                        "                 <br><li>Dès que vous acceptez les termes d'utilisation, VosLiens collecte les informations relatives à votre utilisation" +
                        "                des services et celles qui concernent l’appareil sur lequel vous avez installé VosLiens. Exemple :" +

                        "                <br><li>VosLiens est susceptible de collecter et d’enregistrer la fréquence et la durée de votre utilisation de l’application." +
                        "                <br><li>Les services internes de VosLiens ont évidemment accès aux données que vous publiez (hors mot de passe) et ne sont pas utilisées hors du cadre IV" +
                        " ni même observées (hors dysfonctionnement du système nécessitant une intervention).  "+
                        "<br>"+
                        "                <big><u><br><b>IV - Raison d'utilisation de vos données</b></u></big>" +
                        "                 <br><li>VosLiens collecte vos données dans le but de pouvoir les échanger avec d'autres utilisateurs pour permettre de les parrainer ou d'être" +
                        "                parrainé." +

                        "                <br><li>VosLiens utilise également vos informations pour garantir que nos services fonctionnent comme prévu, notamment en surveillant les pannes ou en résolvant" +
                        "                les problèmes signalés." +
                        "                <br><li>En outre, VosLiens utilise vos informations pour améliorer le service ou pour développer de nouvelles fonctionnalités ou de nouveaux services." +
                        "                <br><li>VosLiens peut être amené à utiliser votre nom , prénom et votre code de parrainage afin de transmettre ces informations à l'entité concernée  (banque, assurance) afin d'être rémunérée." +


                        "<br>"+
                        "               <big> <u><br><b>V- Visibilité</b></u></big>" +

                        "                 <br><li>Veuillez noter que votre nom d’utilisateur et votre score seront publiés." +
                        "                <br><li>Votre nom d’utilisateur et votre score sera par conséquent visible par les autres utilisateurs." +
                        "                <br><li>Veuillez noter que les informations communiquées lors de l'envoi des informations de parrainage seront visible par un utilisateur" +
                        "                choisi au hasard parmi les utilisateurs n'ayant pas expiré leur quota de parrainage pour l'entité donnée." +
                        "               <br><li> Les informations que vous communiquez à ce moment précis seront visibles par cet utilisateur." +
                        "<br>"+

                        "                <big><u><br><b>VI - Informations que vous partagez</b></u></big>" +

                        "                <br><li>L'utilisateur est responsable du partage des données personnelles qu'il émét lorsqu'il souhaite parrainer" +
                        "                <br><li>Ces données partagées peuvent etre visibles par tout utilisateur recevant ses informations de parrainage (telles que nom de famille ,prénom" +
                        "                ou tout autre information nécessaire au parraiange et demandée par l'entité de parrainage)" +
                        "<br>"+
                        "                <big><u><br><b>VI - Devoir du Parrain</b></u></big>" +

                        "                <br><li>VosLiens permet simplement de mettre en relation un parrain et un filleul . " +
                        "Tout utilisateur doit s'informer des conditions générales des entités de parrainage qu'il utilise , disponibles sur le site web de ces derniers" +
                        "<br>"+
                        "               <big><u><br> <b>VII - Publicité :</b></u></big>" +
                        "                <br><li>VosLiens autorise les annonceurs à diffuser des campagnes publicitaires sur ses Services." +

                        "                <br><li>Les informations et les publicités que vous consultez lorsque vous accédez aux Services sont susceptibles" +
                        "                d’être ciblées en fonction de votre activité." +
                        "<br>"+
                        "              <big><u><br> <b>VIII - Suppression de vos informations</b></u></big>" +
                        "                <br><li>Si vous souhaitez supprimer votre Compte, veuillez utiliser la fonction “Supprimer le compte”," +
                        "                 disponible dans le Menu de l’application ." +
                        "                 <br><li>Nous nous efforçons d’initier le processus de suppression immédiatement après avoir reçu votre demande." +
                        "                 <br><li> Pour toute autre demande concernant l'exercice de vos droits, veuillez nous contacter à l'adresse support@vosliens.fr"+
                        "                 <br><li> Les données sont conservées jusqu'à suppression de votre compte ou à la fermeture/liquidation de VosLiens ou des serveurs Amazon"+
                        "                 <br><li> VosLiens peut en outre subir des dysfonctionnements techniques entrainant l'effacement des données"+
                        "<br>"+
                        "               <big><u><br> <b>IX - Licenses utilisées  :</b></u></big>" +
                        "                <br><li>VosLiens utilise les icones du site https://icons8.com et l'expose dans cet article conformément à la license requise."+
        "              <big><br> <u><b>Reglement</u></b></big>" +
                        "<br>"+
                        "              <br> <b>I-Clause de non responsabilité</b>" +
                        "               <br><li>En aucun cas VosLiens ou ses employés ou créateurs ne pourront être tenus responsable par vous ou une tierce personne" +
                        "               pour quelque cause que ce soit résultant de l'utilisation de l'application et de ses services." +
                        "               <br><li>VosLiens se dégage de toute responsabilité, quelle qu'elle soit," +
                        "               pour n'importe quelle conséquence de l'utilisation de ses services , incluant toutes pertes , directe ou indirecte ," +
                        "               financière ou non financière." +

                        "             A tout moment , la politique de confidentialité et le règlement  est visible dans le menu 'Politique de confidentialité et règlement'" +




                        "                        ";


        //need to import android.text.Html class

        //set text style bold, italic and underline from html tag

        tv.setText(Html.fromHtml(str));

        tv.setClickable(true);
        tv.setFocusable(false);


        tv.setMovementMethod(LinkMovementMethod.getInstance());

    }

}
