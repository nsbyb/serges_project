package com.serge_app.bethebanker;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by WFTD- on 28/04/2019.
 */

public class MyExpandableListAdapterTick extends BaseExpandableListAdapter{

    Context context;
    List<String> Institutions;
    Map<String, List<String>> Banks_and_Insurances;

    public MyExpandableListAdapterTick(Context context, List<String> institutions, Map<String, List<String>> banks_and_Insurances) {
        this.context = context;
        Institutions = institutions;
        Banks_and_Insurances = banks_and_Insurances;
    }



    @Override
    public int getGroupCount() {
        return Institutions.size();
    }

    @Override
    public int getChildrenCount(int i) {

        return Banks_and_Insurances.get(Institutions.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return Institutions.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return Banks_and_Insurances.get(Institutions.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

        String group= (String) getGroup(i);

        if (view == null)
        {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_parent_banques,null);
        }


        TextView txtParent = (TextView) view.findViewById(R.id.ListParentTextView);
        txtParent.setText(group);

        //Log.i("txtparent",txtParent.getText().toString());
        return view;
    }

    static ArrayList<ArrayList<Integer>> check_states = new ArrayList<ArrayList<Integer>>();

    public void setChildrenAndValues() {
        //initialize the states to all 0;
        for(int i = 0; i < getGroupCount(); i++) {
            ArrayList<Integer> tmp = new ArrayList<Integer>();
            for(int j = 0; j < getChildrenCount(i) ; j++) {
                tmp.add(0);
            }
            check_states.add(tmp);
        }
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {

        final String Group = (String) getGroup(i);
        String bank = (String) getChild(i,i1);

        if (view == null)
        {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listchild_tick,null);
        }

        final TextView txtchild = (TextView) view.findViewById(R.id.textChildTick);


        txtchild.setText(bank);
        //Log.i("txtchild",txtchild.getText().toString());

        setChildrenAndValues();

        final CheckBox childcb = (CheckBox) view.findViewById(R.id.checkBox);
        if (check_states.get(i).get(i1) == 1) {
            childcb.setChecked(true);
        }else{ childcb.setChecked(false);
        }


        final Integer ii = i;
        final Integer ii1 =i1;
        childcb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (childcb.isChecked()) {
                    check_states.get(ii).set(ii1, 1);


                    Log.i("CHARSEQUENCE",txtchild.getText().toString());

                    String[] parts = txtchild.getText().toString().split(":");

                    String p2 = parts[1];
                    String[] p4 = p2.split(" et");
                    String p6 = p4[0];
                    Log.i("PARTS 5",p6);
                    final String p5 = p6.replaceAll("\\s", "");

                    Util.Upload_Validated("ValidatedCode",p5,Group);

                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("user", p5);
                    //request.put("user",ParseUser.getCurrentUser());

                    String Content = "<b>Félicitations: votre code parrain a été utilisé:"
                            + "<br><li>Utilisateur : " + p5 +
                            "<br><li>Banque : " + Group;


                    final String contentString = Html.fromHtml(Content).toString();



                    ParseCloud.callFunctionInBackground("RequestPrivateInfos", params, new FunctionCallback<HashMap<String, String>>() {
                        public void done(HashMap<String, String> privateinfos, ParseException e) {
                            if (e == null) {


                                if (privateinfos != null) {


                                    if(privateinfos.containsKey("Mail"))
                                    {
                                        final String MailText = privateinfos.get("Mail");

                                        Log.i("Mailtext",MailText);

                                        HashMap<String,String> params = new HashMap<String, String>();



                                        //params.put("username", ParseUser.getCurrentUser().getUsername());
                                        params.put("destinataire",MailText);
                                        params.put("textfield",contentString);



                        ParseCloud.callFunctionInBackground("SendMail",params,  new FunctionCallback<String>() {
                            public void done(String str, ParseException e) {
                                if (e == null) {

                                    Log.i("all","good");

                                } else {
                                    Log.i("ERROR",e.getMessage());


                                }
                            }

                        });
/*


                                            HashMap<String, String> paramss = new HashMap<String, String>();
                                            paramss.put("user", "entrade");

                                            ParseCloud.callFunctionInBackground("RetrieveCredentials",paramss,  new FunctionCallback<HashMap<String, String>>() {
                                                public void done(HashMap<String, String> map, ParseException e) {
                                                    if (e == null) {

                                                        Log.i("map",map.toString());
                                                        Log.i("mailtext",MailText);

                                                        String pwd = map.get("code");
                                                        String mail = map.get("mail");
                                                        try {

                                                            MailSender sender = new MailSender(

                                                                    mail,

                                                                    pwd);


                                                            //sender.addAttachment(Environment.getExternalStorageDirectory().getPath()+"/image.jpg");



                                                            sender.sendMail("Votre code a été utilisé :", contentString,

                                                                    mail,

                                                                    MailText);


                                                        } catch (Exception ex) {

                                                            Log.i("MISTAAAKE", ex.toString());


                                                        }


                                                    } else {
                                                        Log.i("ERROR",e.getMessage());


                                                    }
                                                }

                                            });
*/


                                    }
                                }










                            } else {
                                Log.i("error mess", e.getMessage());
                            }
                        }
                    });








                }else{ check_states.get(ii).set(ii1, 0);
                }
            }
        });

       /* SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        if(preferences.contains("checked") && preferences.getBoolean("checked",false) == true) {
            checkBox.setChecked(true);
        }else {
            checkBox.setChecked(false);

        }
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(checkBox.isChecked()) {
                    editor.putBoolean("checked", true);
                    editor.apply();
                }else{
                    editor.putBoolean("checked", false);
                    editor.apply();
                }
            }
        });*/


        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
