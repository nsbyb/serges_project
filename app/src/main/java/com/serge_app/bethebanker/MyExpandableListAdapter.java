package com.serge_app.bethebanker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

/**
 * Created by WFTD- on 28/04/2019.
 */

public class MyExpandableListAdapter extends BaseExpandableListAdapter{

    Context context;
    List<String> Institutions;
    Map<String, List<String>> Banks_and_Insurances;

    public MyExpandableListAdapter(Context context, List<String> institutions, Map<String, List<String>> banks_and_Insurances) {
        this.context = context;
        Institutions = institutions;
        Banks_and_Insurances = banks_and_Insurances;
    }

    @Override
    public int getGroupCount() {
        return Institutions.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return Banks_and_Insurances.get(Institutions.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return Institutions.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return Banks_and_Insurances.get(Institutions.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

        String group= (String) getGroup(i);
        //Log.i("IIIIIIIIIIIIIII", String.valueOf(i) );
        //Log.i("GROUUUPS", group);




                if (view == null) {
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                    view = inflater.inflate(R.layout.list_parent_banques, null);


            }

        TextView txtParent = (TextView) view.findViewById(R.id.ListParentTextView);
        txtParent.setText(group);

       if(group.equals("Banques"))
       {

           ImageView im = (ImageView) view.findViewById(R.id.imageViewParent);
           im.setImageResource(R.drawable.skyscrapers_free_icon);

       }

        if(group.equals("Assurances"))
        {

            ImageView im = (ImageView) view.findViewById(R.id.imageViewParent);
            im.setImageResource(R.drawable.home_insurance);

        }

        if(group.equals("Loisirs"))
        {

            ImageView im = (ImageView) view.findViewById(R.id.imageViewParent);
            im.setImageResource(R.drawable.leisure_park_icon);

        }

        if(group.equals("Services"))
        {

            ImageView im = (ImageView) view.findViewById(R.id.imageViewParent);
            im.setImageResource(R.drawable.service);

        }



        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {

        String bank = (String) getChild(i,i1);
        //Log.i("I111111111111111", String.valueOf(i1) );
        //Log.i("BANNNNK", bank );
        String group= (String) getGroup(i);




        if(view ==null)
        {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);



                view = inflater.inflate(R.layout.listchild_ing, null);




        }

        TextView txtchild = (TextView) view.findViewById(R.id.textChild);
        txtchild.setText(bank);


        if(group.equals("Assurances")) {
            if (bank.equals("GMF") |bank.equals("AXA")) {
                ImageView im = (ImageView) view.findViewById(R.id.imageViewChild);
                im.setImageResource(R.drawable.vehicle_insurance);


            }
        }

        else if(group.equals("Services")) {
            if (bank.equals("Uber")) {
                ImageView im = (ImageView) view.findViewById(R.id.imageViewChild);
                im.setImageResource(R.drawable.taxi_color);

            }
            if (bank.equals("Bolt")) {
                ImageView im = (ImageView) view.findViewById(R.id.imageViewChild);
                im.setImageResource(R.drawable.taxi_color);

            }
            if (bank.equals("Direct Energie")) {
                ImageView im = (ImageView) view.findViewById(R.id.imageViewChild);
                im.setImageResource(R.drawable.directe);

            }
            if (bank.equals("Deliveroo")) {
                ImageView im = (ImageView) view.findViewById(R.id.imageViewChild);
                im.setImageResource(R.drawable.deliveroo);

            }
      /*      if (bank.equals("Amazon")) {
                ImageView im = (ImageView) view.findViewById(R.id.imageViewChild);
                im.setImageResource(R.drawable.amazon);

            }*/
        }

        else if(group.equals("Loisirs")) {
            if (bank.equals("Fitness Park La Défense")) {
                ImageView im = (ImageView) view.findViewById(R.id.imageViewChild);
                im.setImageResource(R.drawable.fitness);


            }
            if (bank.equals("Saveur Bière")) {
                ImageView im = (ImageView) view.findViewById(R.id.imageViewChild);
                im.setImageResource(R.drawable.beer_color);


            }
        }

        else if(group.equals("Banques")) {
            if (  bank.equals("ING")   | bank.equals("Revolut") | bank.equals("Boursorama")  |
                    bank.equals("Crédit Agricole")| bank.equals("BNP Paribas")|
                     bank.equals("Société Générale") |bank.equals("MAX") |
                    bank.equals("Bourse Direct")|bank.equals("American Express")   ) {
                ImageView im = (ImageView) view.findViewById(R.id.imageViewChild);
                im.setImageResource(R.drawable.bank_merchant);


            }




/*
            if(bank.equals("Revolut") )
            {

                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.listchild_revolut, null);

                ImageView im = (ImageView) view.findViewById(R.id.imageViewChild);
                im.setImageResource(R.drawable.bank_merchant);

                ImageView im2 = (ImageView) view.findViewById(R.id.imageViewChild4);
                im2.setImageResource(R.drawable.thumbup);

                TextView txtchildrev = (TextView) view.findViewById(R.id.textChild);
                txtchildrev.setText(bank);

            }*/
        }





                return view;
        }


    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
