package com.serge_app.bethebanker;

import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.HashMap;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;
import uk.co.deanwild.materialshowcaseview.ShowcaseTooltip;

//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;

public class Reciever_Page extends AppCompatActivity {

    String Coder  =" ";

    String Name =" ";

    String Surname = " ";
    ClipData clip;
    String Bank =" ";

    String LienParrainage=" ";

    String Username = " ";

    String Mail = " ";

    String Ville = " ";

    String Adresse = " ";



    String Content;

    String site;
    public static final String GOOGLECHROME_NAVIGATE_PREFIX = "googlechrome://navigate?url=";

    //Class classer = Reciever_Page.class;



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

       /* ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);*/

        MenuInflater menuInflater = new MenuInflater(this);

        menuInflater.inflate(R.menu.bethebankermenu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {



        if(item.getItemId()==R.id.MainPage)
        {

            Intent graphintent = new Intent(this,ActivityChoice.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Disclaimer)
        {
            Intent graphintent = new Intent(this,PolitiqueConfidentalite.class);
            startActivity(graphintent);

        }

        if(item.getItemId()==R.id.Classement)
        {
            Intent scrollList = new Intent(this,ScrollListActivity.class);
            startActivity(scrollList);


        }

        if(item.getItemId()==R.id.GraphPage)
        {

            Intent graphintent = new Intent(this,GraphActivity.class);
            startActivity(graphintent);

        }

        if(item.getItemId() == R.id.Logout) {

            ParseUser.logOut();

            Intent intent = new Intent(getApplicationContext(),MainActivity.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.Destroy) {

            Intent intent = new Intent(getApplicationContext(),DestroyUser.class);

            startActivity(intent);

        }

        if(item.getItemId() == R.id.RecapReceive) {

            Intent intent = new Intent(getApplicationContext(),RecapMenu.class);

            startActivity(intent);

        }

   /*     if(item.getItemId() == R.id.UploadMail) {

            Intent intent = new Intent(getApplicationContext(),UpdateMail.class);

            startActivity(intent);

        }*/

        return super.onOptionsItemSelected(item);
    }

    public void comp_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ScrollListActivity.class);
        startActivity(graphintent);

    }

    public void graph_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), GraphActivity.class);
        startActivity(graphintent);

    }

    public void settings_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), UpdateMail.class);
        startActivity(graphintent);

    }

    public void mail_method(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), RecapMenu.class);
        startActivity(graphintent);

    }

    public void backarrow(View view)
    {
        Intent graphintent = new Intent(getApplicationContext(), ActivityChoice.class);
        startActivity(graphintent);

    }


    private void presentShowcaseSequence() {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, SHOWCASE_ID);

        sequence.setOnItemShownListener(new MaterialShowcaseSequence.OnSequenceItemShownListener() {
            @Override
            public void onShow(MaterialShowcaseView itemView, int position) {
                //Toast.makeText(itemView.getContext(), "Item #" + position, Toast.LENGTH_SHORT).show();
            }
        });

        ImageView settings = findViewById(R.id.settings);
        ImageView mail = findViewById(R.id.logomail);

        Button button = findViewById(R.id.Launchbutton);
        Button button2 = findViewById(R.id.Launchbutton2);

        sequence.setConfig(config);
        Integer corner = 50;
        ShowcaseTooltip toolTip1 = ShowcaseTooltip.build(this)
                .corner(corner)
                .textColor(Color.parseColor("#FFFFFF"))
                .color(Color.parseColor("#2F4172"))
                .text(" <big><b>J'ai compris </b></big>");

        ShowcaseTooltip toolTip2 = ShowcaseTooltip.build(this)
                .corner(corner)
                .textColor(Color.parseColor("#FFFFFF"))
                .color(Color.parseColor("#2F4172"))
                .text(" <big><b>J'ai compris </b></big>");


        ShowcaseTooltip toolTip3 = ShowcaseTooltip.build(this)
                .corner(corner)
                .textColor(Color.parseColor("#FFFFFF"))
                .color(Color.parseColor("#2F4172"))
                .text(" <big><b>J'ai compris </b></big>");

        ShowcaseTooltip toolTip4 = ShowcaseTooltip.build(this)
                .corner(corner)
                .textColor(Color.parseColor("#FFFFFF"))
                .color(Color.parseColor("#2F4172"))
                .text(" <big><b>J'ai compris </b></big>");



        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)

                        .setTarget(mail)

                        .setContentText("N'oubliez pas de cocher la case correspondant à votre code pour notifier votre parrain ")
                        .withCircleShape()
                        .setShapePadding(20)

                        .setToolTip(toolTip1)
                        .setTooltipMargin(20)
                        .setDismissOnTouch(true)


                        .singleUse(SHOWCASE_ID)
                        .setMaskColour(Color.parseColor("#EDFFFFFF"))
                        .setContentTextColor(Color.parseColor("#004a94"))
                        .setDismissTextColor(Color.parseColor("#004a94"))
                        .build()
        );


        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)

                        .setTarget(settings)

                        .setContentText("Renseignez votre mail pour être notifié lorsque l'on utilise votre code")
                        .withCircleShape()
                        .setShapePadding(20)


                        .setToolTip(toolTip2)
                        .setTooltipMargin(20)
                        .setDismissOnTouch(true)
                        .singleUse(SHOWCASE_ID)
                        .setMaskColour(Color.parseColor("#EDFFFFFF"))
                        .setContentTextColor(Color.parseColor("#004a94"))
                        .setDismissTextColor(Color.parseColor("#004a94"))
                        .build()
        );

        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)

                        .setTarget(button)

                        .setShapePadding(20)
                        .setToolTip(toolTip3)
                        .setTooltipMargin(20)
                        .setDismissOnTouch(true)
                        .singleUse(SHOWCASE_ID)
                        .setContentText("Rendez vous sur le site pour vous inscrire")
                        .withRectangleShape()

                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(button2)

                        .setShapePadding(20)
                        .setTooltipMargin(20)
                        .setDismissOnTouch(true)
                        .setToolTip(toolTip4)

                        .singleUse(SHOWCASE_ID)
                        .setContentText("Envoyez vos informations par mail si vous souhaitez vous inscrire sur votre ordinateur")
                        .withRectangleShape()
                        .build()

        );



        sequence.start();

    }


    private static final String SHOWCASE_ID = "Receiverfirst8";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        Intent intent = getIntent();

        Bank = intent.getStringExtra("Bank");

        Log.i("LOOOOOOG",Bank);




        if(Bank.equals("Boursorama") | Bank.equals("Uber") |Bank.equals("Bolt") |
                Bank.equals("Revolut")| Bank.equals("Saveur Bière")  | Bank.equals("Deliveroo")
                | Bank.equals("MAX") )
        {

            if(Bank.equals("Boursorama") ) {
                LienParrainage = intent.getStringExtra("LienParrainage");
            }
            if(Bank.equals("Deliveroo") ) {
                LienParrainage = intent.getStringExtra("Deliveroo");
            }

            if(Bank.equals("MAX") ) {
                LienParrainage = intent.getStringExtra("MAX");
            }
            if(Bank.equals("Uber") ) {
                LienParrainage = intent.getStringExtra("CodeUber");
            }
            if(Bank.equals("Bolt") ) {
                LienParrainage = intent.getStringExtra("Bolt");
            }

            if(Bank.equals("Revolut"))
            {

                LienParrainage = intent.getStringExtra("FortuneoLink");
            }

            if(Bank.equals("Saveur Bière"))
            {

                LienParrainage = intent.getStringExtra("Saveur Bière");
            }

            Username = intent.getStringExtra("Username").replace("\"","");

            setContentView(R.layout.activity_reciever__page_bourso);
            presentShowcaseSequence();

            EditText text1 = (EditText) findViewById(R.id.ResulttextBanque);

            EditText text2 = (EditText) findViewById(R.id.ResultTextUsername);

            EditText text3 = (EditText) findViewById(R.id.ResultTextLienParrainage);






            text1.setText(Bank);


            text2.setText(Username);

            text3.setText(LienParrainage);



        }

        if(Bank.equals(("ING")))
        {
            setContentView(R.layout.activity_reciever__page_ing);

            presentShowcaseSequence();

            Surname = intent.getStringExtra("Surname").replace("\"","");

            Name = intent.getStringExtra("Name").replace("\"","");

            Coder = intent.getStringExtra("Code").replace("\"","");

            Username = intent.getStringExtra("Username").replace("\"","");

            EditText text1 = (EditText) findViewById(R.id.ResulttextBanque);

            EditText text2 = (EditText) findViewById(R.id.ResultTextNomTrader);

            EditText text3 = (EditText) findViewById(R.id.ResultTextPrenomTrader);

            EditText text4 = (EditText) findViewById(R.id.ResultTextUsername);

            EditText text5 = (EditText) findViewById(R.id.ResultTextLienParrainage);




            text1.setText(Bank);


            text2.setText(Name);

            text3.setText(Surname);



            text4.setText(Username);

            text5.setText(Coder);
        }

        if(Bank.equals(("GMF")) |Bank.equals("Direct Energie"))
        {
            setContentView(R.layout.activity_reciever__page_axa);

            presentShowcaseSequence();

      /*      Surname = intent.getStringExtra("Surname").replace("\"","");

            Name = intent.getStringExtra("Name").replace("\"","");*/

            if(Bank.equals("GMF")) {

                Coder = intent.getStringExtra("CodeGMF").replace("\"", "");
            }
            else
            {
                Coder = intent.getStringExtra("Direct Energie").replace("\"", "");

            }

           Username = intent.getStringExtra("Username").replace("\"","");

           /*  Ville = intent.getStringExtra("Ville").replace("\"","");

            Adresse = intent.getStringExtra("Adresse").replace("\"","");

            Mail = intent.getStringExtra("email").replace("\"","");
*/
            EditText text1 = (EditText) findViewById(R.id.ResulttextBanque);

   /*         EditText text2 = (EditText) findViewById(R.id.ResultTextNomTrader);

            EditText text3 = (EditText) findViewById(R.id.ResultTextPrenomTrader);*/

            EditText text4 = (EditText) findViewById(R.id.ResultTextUsername);

            EditText text5 = (EditText) findViewById(R.id.ResultTextSocietaire);


      /*      EditText text6 = (EditText) findViewById(R.id.ResultTextVille);

            EditText text7 = (EditText) findViewById(R.id.ResultTextAdresse);

            EditText text8 = (EditText) findViewById(R.id.ResultTextMail);*/

            text1.setText(Bank);

/*

            text2.setText(Name);

            text3.setText(Surname);
*/



            text4.setText(Username);

            text5.setText(Coder);

/*            text6.setText(Ville);

            text7.setText(Adresse);

            text8.setText(Mail);*/
        }


        if(Bank.equals(("BNP Paribas")) | Bank.equals("Crédit Agricole") |Bank.equals("Société Générale"))
        {
            setContentView(R.layout.activity_reciever__page_bnp);

            presentShowcaseSequence();

            Surname = intent.getStringExtra("Surname").replace("\"","");

            Name = intent.getStringExtra("Name").replace("\"","");

            Mail = intent.getStringExtra("email").replace("\"","");

            Username = intent.getStringExtra("Username").replace("\"","");

            EditText text1 = (EditText) findViewById(R.id.ResulttextBanque);

            EditText text2 = (EditText) findViewById(R.id.ResultTextNomTrader);

            EditText text3 = (EditText) findViewById(R.id.ResultTextPrenomTrader);

            EditText text4 = (EditText) findViewById(R.id.ResultTextLienParrainage);

            EditText text5 = (EditText) findViewById(R.id.ResultTextUsername);



            text1.setText(Bank);



            text2.setText(Name);

            text3.setText(Surname);

            text5.setText(Mail);

            text4.setText(Username);
        }
        if(Bank.equals("Fitness Park La Défense") )
        {
            setContentView(R.layout.activity_reciever__page_fitness);

            presentShowcaseSequence();

            Surname = intent.getStringExtra("FitnessParkSurname").replace("\"","");

            Name = intent.getStringExtra("FitnessParkName").replace("\"","");


            Username = intent.getStringExtra("Username").replace("\"","");

            EditText text1 = (EditText) findViewById(R.id.ResulttextBanque);

            EditText text2 = (EditText) findViewById(R.id.ResultTextNomTrader);

            EditText text3 = (EditText) findViewById(R.id.ResultTextPrenomTrader);



            EditText text5 = (EditText) findViewById(R.id.ResultTextUsername);



            text1.setText(Bank);



            text2.setText(Name);

            text3.setText(Surname);



            text5.setText(Username);
        }
        if(Bank.equals("AXA") | Bank.equals("Bourse Direct") | Bank.equals("American Express") ) {


            setContentView(R.layout.activity_reciever_assurance_axa);
            presentShowcaseSequence();
            if (Bank.equals("AXA")) {

                Mail = intent.getStringExtra("AXA");
            }

            if (Bank.equals("American Express")) {

                Mail = intent.getStringExtra("American Express");
            }

            if (Bank.equals("Bourse Direct"))
            {

                Mail = intent.getStringExtra("Bourse Direct");
            }

            Username = intent.getStringExtra("Username").replace("\"","");

            EditText text1 = (EditText) findViewById(R.id.ResulttextBanque);

            EditText text2 = (EditText) findViewById(R.id.ResultTextMail);

            EditText text3 = (EditText) findViewById(R.id.ResultTextUsername);



            text1.setText(Bank);


            text2.setText(Mail);

            text3.setText(Username);
        }





    }







    public void SendMail(View view )
    {


        final AlertDialog.Builder builder = new AlertDialog.Builder(Reciever_Page.this);

             //builder.setMessage("Renseignez vos details");

        builder.setTitle("Entrez votre adresse mail");

        builder.setCancelable(true);

        LayoutInflater Mailinflater = (LayoutInflater) Reciever_Page.this.getSystemService(Reciever_Page.this.LAYOUT_INFLATER_SERVICE);



            //In case it gives you an error for setView(View) try
        final View mailview = Mailinflater.inflate(R.layout.mailview, null);

        builder.setView(mailview);

        final EditText MailText = (EditText) mailview.findViewById(R.id.LinkMail) ;




        if (Bank.equals("BNP Paribas") )
        {
            site = "http://mabanque.bnpparibas/fr/ma-banque-et-moi/parrainage";

        }
        if (Bank.equals("American Express") )
        {
            site = "https://www.americanexpress.com/fr/";

        }


        if (Bank.equals("Bourse Direct") )
        {
            site = "https://www.boursedirect.fr/fr/bourse/ouvrir-un-compte";

        }
        if (Bank.equals("ING") )
        {
            site = "https://formulaire.ing.fr/public/parrainage/index.jsf";

        }
        if (Bank.equals("Boursorama") )
        {
            site = "https://clients.boursorama.com/connexion/?org=/parrainages";

        }
        if (Bank.equals("Revolut") )
        {
            site = "https://revolut.ngih.net/Rr3Rg";

        }

        if (Bank.equals("Uber") )
        {
            site = "http://www.uber.com/fr/fr/";

        }

        if (Bank.equals("Bolt") )
        {
            site = "https://bolt.eu/fr/";

        }
        if (Bank.equals("Deliveroo") )
        {
            site = "https://deliveroo.fr/fr/";

        }

        if (Bank.equals("Fitness Park La Défense") )
        {
            site = "https://www.fitnesspark.fr/parrainage/";

        }

        if (Bank.equals("Saveur Bière") )
        {
            site = "https://www.saveur-biere.com/fr/";

        }


        if (Bank.equals("Société Générale") ) {
            site =

                    "http://www.societegenerale.com/fr/connaitre-notre-entreprise/engagement/citoyennete/parrainage";
        }

        if (Bank.equals("Crédit Agricole") )
        {
            site = "http://www.credit-agricole.fr/ca-centrefrance/particulier.html";

        }

        if (Bank.equals("GMF") )
        {
            site = "https://www.gmf.fr/parrainage";

        }

        if (Bank.equals("Direct Energie") )
        {
            site = "https://total.direct-energie.com/particuliers/pourquoi-nous-choisir/5-bonnes-raisons-de-nous-choisir/parrainer-ses-proches";

        }



        if (Bank.equals("MAX") )
        {
            site = "https://www.max.fr";

        }


        if (Bank.equals("AXA") )
        {
            site = "https://www.axa.fr/assurance-auto.html";

        }

        builder.setPositiveButton("Envoyer les informations par mail", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {




                if(Bank.equals("Boursorama") | Bank.equals("Revolut") | Bank.equals("Bolt")| Bank.equals("Deliveroo")|
                        Bank.equals("Uber")| Bank.equals("Saveur Bière")| Bank.equals("MAX"))
                {
                    Coder = LienParrainage;

                    Content = "<b>Les Informations de votre parrain sont :"
                            + "<br><li> Lien de parrainnage : "+Coder +"<br><li> Lien vers le site : "+site;
                }
                if(Bank.equals("AXA") |Bank.equals("Bourse Direct") | Bank.equals("American Express"))
                {
                    Coder = Mail;

                    Content = "<b>Les Informations de votre parrain sont :"
                            + "<br><li> Lien de parrainnage : "+Coder +"<br><li> Lien vers le site : "+site;
                }

                if(Bank.equals("ING"))
                {

                    Content = "<b>Les Informations de votre parrain sont :"
                            + "<br><li>Nom : " +Name +"<br><li> Prénom : " +Surname +
                            "<br><li>Code : " + Coder +"<br><li> Lien vers le site : "+site;
                }

                if(Bank.equals("Fitness Park La Défense"))
                {

                    Content = "<b>Les Informations de votre parrain sont :"
                            + "<br><li>Nom : " +Name +"<br><li> Prénom : " +Surname +
                            "<br><li> Lien vers le site : "+site;
                }


                if(Bank.equals(("BNP Paribas")) | Bank.equals("Crédit Agricole") |Bank.equals("Société Générale") |Bank.equals("GMF") | Bank.equals("Direct Energie"))
                {




                    Content = "<b>Les Informations de votre parrain sont :"+
                            "<br><li>Code : " + Coder +"<br><li> Lien vers le site : "+site;

                }






                String contentString = Html.fromHtml(Content).toString();


                HashMap<String,String> params = new HashMap<String, String>();



                //params.put("username", ParseUser.getCurrentUser().getUsername());
                params.put("destinataire",MailText.getText().toString());
                params.put("textfield", contentString);



                ParseCloud.callFunctionInBackground("SendMail",params,  new FunctionCallback<String>() {
                    public void done(String str, ParseException e) {
                        if (e == null) {

                            Toast.makeText(getApplicationContext(),"Mail envoyé avec succès",Toast.LENGTH_LONG).show();

                        } else {



                        }
                    }

                });




            }
        });
        builder.setNegativeButton("Annuler",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder.show();
        builder.create();

    }











    public void LaunchAppMethod(View view)

    {



        AlertDialog.Builder builder = new AlertDialog.Builder(Reciever_Page.this);

        builder.setTitle("Interface de Lancement");

        builder.setMessage("Vérifiez que vous avez bien Chrome installé.");

        builder.setCancelable(true);

        builder.setPositiveButton("Aller sur le site de " + Bank, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                ClipboardManager clipboard = (ClipboardManager) getSystemService(getApplicationContext().CLIPBOARD_SERVICE);



                if(Bank.equals("Boursorama") | Bank.equals("Revolut") | Bank.equals("Bolt")|Bank.equals("Deliveroo")|
                        Bank.equals("Uber")| Bank.equals("Saveur Bière") | Bank.equals("MAX"))
                {
                    Coder = LienParrainage;
                    ClipData clip = ClipData.newPlainText("Code","Code : " +Coder );
                    clipboard.setPrimaryClip(clip);
                }

                if(Bank.equals("AXA") |Bank.equals("Bourse Direct") | Bank.equals("American Express"))
                {
                    Coder = Mail;
                    ClipData clip = ClipData.newPlainText("Code","Mail : " +Coder );
                    clipboard.setPrimaryClip(clip);
                }

          
                if(Bank.equals("ING"))
                {
                    ClipData clip = ClipData.newPlainText("Code","Code : " +Coder + " Nom : "+ Name + " Prénom :" + Surname);

                    //clip.addItem(new ClipData.Item("",Name));

                    //clip.addItem(new ClipData.Item("",Surname));

                    clipboard.setPrimaryClip(clip);
                }

                if(Bank.equals("Fitness Park La Défense"))
                {
                    ClipData clip = ClipData.newPlainText("Code"," Nom : "+ Name + " Prénom :" + Surname);

                    //clip.addItem(new ClipData.Item("",Name));

                    //clip.addItem(new ClipData.Item("",Surname));

                    clipboard.setPrimaryClip(clip);
                }



                if(Bank.equals("GMF")|
                        Bank.equals("Direct Energie"))
                {

                    //Coder = Mail;

                    ClipData clip = ClipData.newPlainText("Code","Code : " +Coder );

                    //clip.addItem(new ClipData.Item("",Name));

                    //clip.addItem(new ClipData.Item("",Surname));



                    clipboard.setPrimaryClip(clip);

                }




                Toast.makeText(getApplicationContext(),"Data copied to Clipboard",Toast.LENGTH_LONG).show();

                if (Bank.equals("ING")){

                    Uri webpage = Uri.parse("https://formulaire.ing.fr/public/parrainage/index.jsf");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }



                }

                if (Bank.equals("American Express")){

                    Uri webpage = Uri.parse("https://www.americanexpress.com/fr/");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }

                }


                if (Bank.equals("Fitness Park La Défense")){

                    Uri webpage = Uri.parse("https://www.fitnesspark.fr/parrainage/");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }

                }

                if (Bank.equals("GMF")){

                    Uri webpage = Uri.parse("https://www.gmf.fr/parrainage");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }

                }

                if (Bank.equals("Bourse Direct")){

                    Uri webpage = Uri.parse("https://www.boursedirect.fr/fr/bourse/ouvrir-un-compte");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }

                }

                if (Bank.equals("Direct Energie")){

                    Uri webpage = Uri.parse("https://total.direct-energie.com/particuliers/pourquoi-nous-choisir/5-bonnes-raisons-de-nous-choisir/parrainer-ses-proches");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }



                }


                if (Bank.equals("MAX")){

                    Uri webpage = Uri.parse("https://www.max.fr/");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }



                }
                if (Bank.equals("AXA")){

                    Uri webpage = Uri.parse("https://www.axa.fr/assurance-auto.html");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }



                }

                if(Bank.equals("Boursorama"))

                {


                    try {
                        try {
                            Uri uri = Uri.parse("googlechrome://navigate?url="+ "https://clients.boursorama.com/connexion/?org=/parrainages");
                            Intent webintent = new Intent(Intent.ACTION_VIEW, uri);
                            webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            Log.i("Uri",uri.toString());
                            startActivity(webintent);
                        } catch (ActivityNotFoundException e) {
                            Log.i("exception",e.toString());
                            Uri uri = Uri.parse("https://clients.boursorama.com/connexion/?org=/parrainages");
                            // Chrome is probably not installed
                            // OR not selected as default browser OR if no Browser is selected as default browser
                            Intent webintent = new Intent(Intent.ACTION_VIEW, uri);
                            webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(webintent);
                        }
                    } catch (Exception ex) {
                        Toast.makeText(getApplicationContext(),ex.getMessage().toString(),Toast.LENGTH_LONG).show();;
                    }



                }

                if(Bank.equals("Crédit Agricole"))

                {

                    Uri webpage = Uri.parse("http://www.credit-agricole.fr/ca-centrefrance/particulier.html");



                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }


                }

                if(Bank.equals("Société Générale"))

                {

                    Uri webpage = Uri.parse("http://www.societegenerale.com/fr/connaitre-notre-entreprise/engagement/citoyennete/parrainage");

                    //Uri webpage = Uri.parse("https://www.google.com/maps");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }


                }

                if(Bank.equals("BNP Paribas"))
                {
                    Uri webpage = Uri.parse("http://mabanque.bnpparibas/fr/ma-banque-et-moi/parrainage");

                    //Uri webpage = Uri.parse("https://www.google.com/maps");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }

                }

                if(Bank.equals("Revolut"))
                {
                    Uri webpage = Uri.parse("https://revolut.ngih.net/Rr3Rg");

                    //Uri webpage = Uri.parse("https://www.google.com/maps");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }

                }

                if(Bank.equals("Saveur Bière"))
                {
                    Uri webpage = Uri.parse("https://www.saveur-biere.com/fr/");

                    //Uri webpage = Uri.parse("https://www.google.com/maps");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }

                }


                if(Bank.equals( "Uber"))
                {
                    Uri webpage = Uri.parse("http://www.uber.com/fr/fr/");

                    //Uri webpage = Uri.parse("https://www.google.com/maps");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }



                }


                if(Bank.equals( "Bolt"))
                {
                    Uri webpage = Uri.parse("https://bolt.eu/fr/");

                    //Uri webpage = Uri.parse("https://www.google.com/maps");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }



                }


                if(Bank.equals( "Deliveroo"))
                {
                    Uri webpage = Uri.parse("https://deliveroo.fr/fr/");

                    //Uri webpage = Uri.parse("https://www.google.com/maps");

                    Intent webintent = new Intent(Intent.ACTION_VIEW,webpage);

                    webintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    webintent.setPackage("com.android.chrome");
                    try {
                        startActivity(webintent);
                    } catch (ActivityNotFoundException e) {
                        // Chrome is probably not installed
                        // Try with the default browser
                        webintent.setPackage(null);
                        startActivity(webintent);
                    }



                }





            }
        });

        builder.setNegativeButton("Annuler",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder.show();
        builder.create();





    }
}
