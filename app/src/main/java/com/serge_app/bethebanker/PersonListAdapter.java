package com.serge_app.bethebanker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;

//import android.support.annotation.NonNull;

/**
 * Created by WFTD- on 12/05/2019.
 */

public class PersonListAdapter extends ArrayAdapter<Person>{

    private Context mContext ;
    private int mResource;
    private int last_position =-1;

    static class ViewHolder{
        TextView name;

        ImageView logo;

        TextView score;

        TextView positionscore;

    }


    public PersonListAdapter(Context context, int resource, ArrayList<Person> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        setUpImageLoader();
        String name = getItem(position).getName();
        Integer score= getItem(position).getScore();
        String imageurl = getItem(position).getUri();

        Integer positionscore = getItem(position).getPositionscore();


        Person person = new Person(name,score,imageurl,positionscore);



        final View result;
        ViewHolder holder ;

        if(convertView == null)
        {

            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource,parent,false);

            holder = new ViewHolder();
            holder.logo = (ImageView) convertView.findViewById(R.id.logo_id);

/*
            holder.logo.getLayoutParams().height = 200;

            holder.logo.getLayoutParams().width = 200;*/


            holder.name = (TextView) convertView.findViewById(R.id.id_scroller);
            holder.score = (TextView) convertView.findViewById(R.id.id_scroller2);
            holder.positionscore = convertView.findViewById(R.id.ScoreOrder);
            result = convertView;
            convertView.setTag(holder);
        }

        else
        {
            holder = (ViewHolder) convertView.getTag();
            result = convertView;

        }


        Animation animation = AnimationUtils.loadAnimation(mContext, position>last_position ?
        R.anim.load_down_anim :R.anim.load_up_anim);
        result.startAnimation(animation);
        last_position = position;

        int defaultimage = mContext.getResources().getIdentifier("@drawable/greydoll.jpg",null,mContext.getPackageName());

        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(defaultimage)
                .showImageOnFail(defaultimage)
                .showImageOnLoading(defaultimage).build();


        holder.name.setText(name);
        holder.score.setText(String.valueOf(score));
        holder.positionscore.setText(String.valueOf(positionscore));
        imageLoader.displayImage(imageurl,holder.logo,options);


/*
        TextView tvname =(TextView) convertView.findViewById(R.id.id_scroller);
        TextView tvscore = (TextView) convertView.findViewById(R.id.id_scroller2);
        ImageView tvimage =(ImageView) convertView.findViewById(R.id.logo_id);*/

      /*  tvname.setText(name);
        tvscore.setText(score);
        tvimage.setImageURI(imageurl);*/

        return  convertView;



    }

    private void setUpImageLoader()
    {

        DisplayImageOptions defaultoption = new DisplayImageOptions.Builder().
                cacheOnDisk(true).cacheInMemory(true).imageScaleType(ImageScaleType.EXACTLY).
                displayer( new FadeInBitmapDisplayer(300)).build();


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext).defaultDisplayImageOptions(defaultoption).
                                                memoryCache(new WeakMemoryCache()).diskCacheSize(100*1024*1024).build();

        ImageLoader.getInstance().init(config);







    }
}
